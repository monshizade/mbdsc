/**
 * The Prediction-based Overload Detection Algorithm (POD)
 * 
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 * 
 * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 * 
 * @author H. Monshizadeh Naeen
 */
package org.cloudbus.cloudsim.power;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.examples.power.Constants;
import org.cloudbus.cloudsim.util.CUSUMs;
import org.cloudbus.cloudsim.util.MarkovChains;

public class MarkovVmAllocationPolicyMigrationPOD extends PowerVmAllocationPolicyMigrationAbstract {

	/** The Allowable OTF. */
	private double AllowableOTF = 0.1;
        
	/** The scheduling interval. */
	private double schedulingInterval= Constants.SCHEDULING_INTERVAL;


	/**
	 * Instantiates a new power vm allocation policy migration mad.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 * @param utilizationThreshold the utilization threshold
	 */
	public MarkovVmAllocationPolicyMigrationPOD(
			List<? extends Host> hostList,
			PowerVmSelectionPolicy vmSelectionPolicy,
			double utilizationThreshold) {
		super(hostList, vmSelectionPolicy);
		setAllowableOTF(AllowableOTF);
	}
	/**
	 * Checks if is host over utilized.
	 * 
	 * @param _host the _host
	 * @return true, if is host over utilized
	 */
	@Override
	protected boolean isHostOverUtilized(PowerHost _host) {
		double totalRequestedMips = 0;
		for (Vm vm : _host.getVmList()) {
			totalRequestedMips += vm.getCurrentRequestedTotalMips();
		}
		double utilization = totalRequestedMips / _host.getTotalMips();
                //---------Gets OTF Value on the host-->>>
		double slaViolationTimePerHost = 0;
		double totalTime = 0;
                double ro=getAllowableOTF();
                if(utilization > 1){
			HostDynamicWorkload host = (HostDynamicWorkload) _host;
			double previousTime = -1;
			double previousAllocated = 0;
			double previousRequested = 0;
			boolean previousIsActive = true;
			for (HostStateHistoryEntry entry : host.getStateHistory()) {
				if (previousTime != -1 && previousIsActive) {
					double timeDiff = entry.getTime() - previousTime;
					totalTime += timeDiff;
                                    	if (previousAllocated < previousRequested) {
                                    		slaViolationTimePerHost += timeDiff;
					}
				}
				previousAllocated = entry.getAllocatedMips();
				previousRequested = entry.getRequestedMips();
				previousTime = entry.getTime();
				previousIsActive = entry.isActive();
			}		
                        addHistoryEntry(host, slaViolationTimePerHost);   
                }
                else{
                         PowerHostUtilizationHistory host = (PowerHostUtilizationHistory) _host;                      
		double[] utilizationHistory = host.getUtilizationHistory();
		int length =utilizationHistory.length; //10; // we use 10 to make the regression responsive enough to latest values
		double[] utilizationHistoryReversed = new double[length];
		for (int i = 0; i < length; i++) {
			utilizationHistoryReversed[i] = utilizationHistory[length - i - 1];
		}				
                int[] step_stLen_hsLen_lp=CUSUMs.getScaleCUSUMsData(utilizationHistoryReversed);
                        int chP=step_stLen_hsLen_lp[0];
                        int stLen=step_stLen_hsLen_lp[1];
                        int hsLen=step_stLen_hsLen_lp[2];
                        int lp=step_stLen_hsLen_lp[3];
                        if(stLen<lp)
                        { //prediction with high accuracy cannot be done so we use deffered policy
                            return false;
                        }
                //--------------------creating the Marcov chain from the last change point to now
                double[] NewUtilizationHistoryAfterChangePoint =CUSUMs.selectArrFrom(utilizationHistoryReversed, chP, hsLen);
                int N=11;//emperical results showed that 11 states have a good result
                boolean predictedOverutilizedN=true;
                int[] obsN=MarkovChains.make_n_state_observation_sequence(NewUtilizationHistoryAfterChangePoint, N);
                double[][] transitionPbMatrixN=MarkovChains.transitionProbabilityMatrix(obsN, N);
                double[] currentUtilization={utilization};
                int[] currentState=MarkovChains.make_n_state_observation_sequence(currentUtilization, N);
                double pbOverload=transitionPbMatrixN[currentState[0]][(N-1)];
                for(int i=0;i<N-1;i++)
                {
                    if(pbOverload<transitionPbMatrixN[currentState[0]][i])
                        predictedOverutilizedN=false;
                }
                if(predictedOverutilizedN)
                {
                    double migrationIntervals = Math.ceil(getMaximumVmMigrationTime(_host) / getSchedulingInterval());
                    totalTime+=getSchedulingInterval()+migrationIntervals;
                    slaViolationTimePerHost+=getSchedulingInterval()+migrationIntervals;
                    HostDynamicWorkload host1 = (HostDynamicWorkload) _host;
			double previousTime = -1;
			double previousAllocated = 0;
			double previousRequested = 0;
			boolean previousIsActive = true;
			for (HostStateHistoryEntry entry : host1.getStateHistory()) {
				if (previousTime != -1 && previousIsActive) {
					double timeDiff = entry.getTime() - previousTime;
					totalTime += timeDiff;                                        
					if (previousAllocated < previousRequested) {                                      
						slaViolationTimePerHost += timeDiff;                                               
					}
				}
				previousAllocated = entry.getAllocatedMips();
				previousRequested = entry.getRequestedMips();
				previousTime = entry.getTime();
				previousIsActive = entry.isActive();
			}	
                addHistoryEntry(host1, slaViolationTimePerHost);
                }
                  }
        return (slaViolationTimePerHost / totalTime)>ro;
        }

	/**
	 * Sets the utilization threshold.
	 * 
	 * @param utilizationThreshold the new utilization threshold
	 */
	protected void setAllowableOTF(double AllowableOTF) {
		this.AllowableOTF = AllowableOTF;
	}

	/**
	 * Gets the utilization threshold.
	 * 
	 * @return the utilization threshold
	 */
	protected double getAllowableOTF() {
		return AllowableOTF;
	}


	/**
	 * Gets the scheduling interval.
	 * 
	 * @return the scheduling interval
	 */
	protected double getSchedulingInterval() {
		return schedulingInterval;
	}
        /**
	 * Gets the maximum vm migration time.
	 * 
	 * @param host the host
	 * @return the maximum vm migration time
	 */
        protected double getMaximumVmMigrationTime(PowerHost host) {
		int maxRam = Integer.MIN_VALUE;
		for (Vm vm : host.getVmList()) {
			int ram = vm.getRam();
			if (ram > maxRam) {
				maxRam = ram;
			}
		}
		return maxRam / ((double) host.getBw() / (2 * 8000));
	}

}
