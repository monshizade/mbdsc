/**
 * The abstract class of Markov based VM allocation policy that dynamically optimizes the VM
 * allocation using migration.
 * The class includes EQBFD VM placement, QPMs detection, CUSUM-based Underloaded hosts detection, and other related algorithms
 * 
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 * 
 * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 *  
 * @author H. Monshizadeh Naeen
 */

package org.cloudbus.cloudsim.power;


import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.apache.commons.math3.stat.StatUtils;
import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.util.CUSUMs;
import org.cloudbus.cloudsim.util.MarkovChains;


public abstract class MarkovBasedVmAllocationPolicy extends PowerVmAllocationPolicyMigrationAbstract {
        public static double AllowableOTF=0.1;
	private final Map<Integer, double[]> TargetParams = new HashMap<Integer, double[]>();
	/**
	 * Instantiates a new power vm allocation policy migration abstract.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 */
	public MarkovBasedVmAllocationPolicy(
			List<? extends Host> hostList,
			PowerVmSelectionPolicy vmSelectionPolicy) {
		super(hostList,vmSelectionPolicy);
		setVmSelectionPolicy(vmSelectionPolicy);
	}

	/**
	 * Find host for vm.
	 * 
	 * @param vm the vm
	 * @param excludedHosts the excluded hosts
	 * @return the power host
	 */
        @Override
	public PowerHost findHostForVm(Vm vm, Set<? extends Host> excludedHosts) {
               return(EQBFD(vm, excludedHosts));

	}
                     
        /**
	 * EQBFD (Energy-efficient and Qos_aware BFD) algorithm for VM Placement
	 * 
	 * @param vm the vm
	 * @param excludedHosts the excluded hosts
	 * @return the new vm placement
*/
	public PowerHost EQBFD(Vm vm, Set<? extends Host> excludedHosts) {
                if (histLen<30)// we assume that at least 2.5 hour of utilization history is needed to start EQBFD
                    return(fallbackVMPlacementPolicy(vm, excludedHosts));
		double minResidual = Double.MAX_VALUE;
                double MaxEff = 0;
                double totalMips;
                double residual;
                double[] AfterAllocationParams={0,0};
		PowerHost allocatedHost = null;

		for (PowerHost host : this.<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			
                        if(true){
				if (getUtilizationOfCpuMips(host) != 0 && isHostOverUtilizedAfterAllocation(host, vm)) {                               
					continue;
				}                                  
				try {
					double powerEfficiancy = getHostPowerEff(host);
					if (powerEfficiancy != -1) {
                                               AfterAllocationParams=getOTFandAvailableCapacityAfterallocation(host, vm);
                                               totalMips=host.getTotalMips(); 
                                               double meanAC=AfterAllocationParams[0];
                                               meanAC=meanAC*totalMips;
                                               residual=totalMips-meanAC;
                                               boolean IsQPM=isQPM(AfterAllocationParams);                                               
                                               if (powerEfficiancy > MaxEff && IsQPM) {
							MaxEff = powerEfficiancy;
                                                        minResidual=residual;
							allocatedHost = host;
						}
                                               else if(MaxEff==powerEfficiancy && residual < minResidual && IsQPM)
                                               {
                                                        minResidual = residual;
							allocatedHost = host;
                                               }
					}
				} catch (Exception e) {
				}
			}
		}
		return allocatedHost;		
	}
         @Override
	protected PowerHost getUnderUtilizedHost(Set<? extends Host> excludedHosts) {
		double minUtilization = Double.MAX_VALUE;
		PowerHost underUtilizedHost = null;
                boolean negativeStep=false;
		for (PowerHost host : this.<PowerHost> getHostList()) {
                    double utilizationMean=getUtilizationMean(host);
                    PowerHostUtilizationHistory _host=(PowerHostUtilizationHistory)host;
                    try{
                    if(TargetParams.containsKey(_host.getId()))
                    {
                        boolean noStep=true;
                        double[] currentUtilizationHistory=_host.getUtilizationHistory();
                        double[] hostTargetParams=TargetParams.get(_host.getId());
                        currentUtilizationHistory=CUSUMs.reverseArr(currentUtilizationHistory);
                        int stepPoint=(int)hostTargetParams[0];
                        double[] NewUtilizationHistory =CUSUMs.selectArrFrom(currentUtilizationHistory, stepPoint, currentUtilizationHistory.length);
                        int[] step_stLen_hsLen_lp=CUSUMs.getStandardizeCUSUMsData(NewUtilizationHistory);
                        int chP=step_stLen_hsLen_lp[0];
                        utilizationMean=StatUtils.mean(NewUtilizationHistory);
                        if(chP!=0)
                            noStep=false;
                        if(!noStep)
                            if(utilizationMean<hostTargetParams[1])
                                negativeStep=true;                                                 
                    }
                    }catch (Exception e){
                        getFallbackUnderUtilizedHost(excludedHosts);
                    } 
			if (excludedHosts.contains(host)||(utilizationMean>0.50&&!negativeStep)) {
				continue;
			}
			double utilization = host.getUtilizationOfCpu();
			if (utilization > 0 && utilization < minUtilization
					&& !areAllVmsMigratingOutOrAnyVmMigratingIn(host)) {
				minUtilization = utilization;
				underUtilizedHost = host;
			}
		}
		return underUtilizedHost;
	}
        public static double[][] OnlineTransitionMatrixEstimator(double[] utilizationHistory)
        {
			N=11;//emperical results showed that 11 states have a good result
            double[] CurrentStationaryUtilizationHistoryAfterAllocation =CUSUMs.getCurrentStationaryWorkload(utilizationHistory);
            int[] obs=MarkovChains.make_n_state_observation_sequence(CurrentStationaryUtilizationHistoryAfterAllocation, N);
            double[][] transitionPbMatrix=MarkovChains.transitionProbabilityMatrix(obs, N);
            return transitionPbMatrix;
        }
        public PowerHost fallbackVMPlacementPolicy(Vm vm, Set<? extends Host> excludedHosts)
        {
                double minPower = Double.MAX_VALUE;
		PowerHost allocatedHost = null;

		for (PowerHost host : this.<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			if (host.isSuitableForVm(vm)) {
                                if (getUtilizationOfCpuMips(host) != 0 && isHostOverUtilizedAfterAllocation(host, vm)) {
					continue;
				}
				try {
					double powerAfterAllocation = getPowerAfterAllocation(host, vm);
					if (powerAfterAllocation != -1) {
						double powerDiff = powerAfterAllocation - host.getPower();
						if (powerDiff < minPower) {
							minPower = powerDiff;
							allocatedHost = host;
						}
					}
				} catch (Exception e) {
				}
			}
		}
		return allocatedHost;

        }
        /**
	 * Check if host is a QOM.
	 * 
	 * @param gets the parameters of utilization after allocation
	 * @return true if host is a QPM
	*/
        public boolean isQPM(double[] AfterAllocationParams)
        {
            double PredictedOTF=AfterAllocationParams[1];
            return (PredictedOTF<=AllowableOTF);
        }
	/**
	 * Find host for vm.
	 * 
	 * @param vm the vm
	 * @return the power host
	 */
	@Override
	public PowerHost findHostForVm(Vm vm) {
		Set<Host> excludedHosts = new HashSet<Host>();
		if (vm.getHost() != null) {
			excludedHosts.add(vm.getHost());
		}
		return findHostForVm(vm, excludedHosts);
	}

        protected PowerHost getFallbackUnderUtilizedHost(Set<? extends Host> excludedHosts) {
		double minUtilization = 1;
		PowerHost underUtilizedHost = null;
		for (PowerHost host : this.<PowerHost> getHostList()) {
			if (excludedHosts.contains(host)) {
				continue;
			}
			double utilization = host.getUtilizationOfCpu();
			if (utilization > 0 && utilization < minUtilization
					&& !areAllVmsMigratingOutOrAnyVmMigratingIn(host)) {
				minUtilization = utilization;
				underUtilizedHost = host;
			}
		}
		return underUtilizedHost;
	}
	/**
	 * Gets the under utilized host.
	 * 
	 * @param excludedHosts the excluded hosts
	 * @return the under utilized host
	 *
           
	/**
        * get host OTF and Available Capacity of a Host after the allocation of a VM 
        * 
        * @param host the destination host
        * @param vm the migrating vm
        * @return the Estimated OTF and mean Available Capacity
        */      
        protected double[] getOTFandAvailableCapacityAfterallocation(PowerHost host, Vm vm) {
               double[] res = new double[2];
               if (host.vmCreate(vm)) {
			PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host;
                        double[] utilizationHistoryAfterAllocation = _host.getUtilizationHistory();
                        utilizationHistoryAfterAllocation=CUSUMs.reverseArr(utilizationHistoryAfterAllocation);
                        host.vmDestroy(vm);
                        double[][] transitionPbMatrix=OnlineTransitionMatrixEstimator(utilizationHistoryAfterAllocation);
                        double[][] St=MarkovChains.StationaryDistribution_PowerMethod(transitionPbMatrix, 11);
                        double OTFAfterAllocation=St[2][0];
                        res[0]=GetCurrentUtilizationMeanAvailableCpacity(utilizationHistoryAfterAllocation);
                        res[1]=OTFAfterAllocation;                   
                        
                }
                return res;                 

    }
        /**
        * get the mean of the current stationary utilization
        * 
        * @param utilizationHistory 
        */  
        public static double GetCurrentUtilizationMeanAvailableCpacity(double[] utilizationHistory)
        {
            double[] CurrentStationaryUtilizationHistoryAfterAllocation =CUSUMs.getCurrentStationaryWorkload(utilizationHistory);
            double mu=StatUtils.mean(CurrentStationaryUtilizationHistoryAfterAllocation);
            return mu;
        }
        /**
        * get a Host's Power efficiency
        * 
        * @param powerHost 
        */    
        protected double getHostPowerEff(PowerHost host) {
		double eff = 0;
		try {
                    int level=0;
                    double sum=0;
                    while(level<=100)
                    {
                    sum+=((host.getTotalMips()*(double)level/100)/host.getPowerModel().getPower((double)level/100));                    
                    level+=10;                    
                    }
                    eff = sum/11;//there are 11 levels of utilizations
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
		return eff;
	}
        /**
        * get a Host's utilization mean 
        * 
        * @param host 
        */     
        protected double getUtilizationMean(PowerHost host) {
            PowerHostUtilizationHistory _host = (PowerHostUtilizationHistory) host;
            double[] utilizationHistory = _host.getUtilizationHistory();
            double expectedValue=StatUtils.mean(utilizationHistory); 
            return expectedValue;
	}
        /**
        * Sets Hosts' stable state history
        * 
        * @param migrationMap 
        */     
        protected void setTargetParams(List<Map<String, Object>> migrationMap)
        {           					
            for (Map<String, Object> migrate : migrationMap) {
                    Vm vm = (Vm) migrate.get("vm");                                                                                          
                    PowerHost targetHost = (PowerHost) migrate.get("host");targetHost.vmCreate(vm);
                    PowerHostUtilizationHistory dstHost = (PowerHostUtilizationHistory) targetHost;
                    TargetParams.put(dstHost.getId(), dstHost.getUtilizationHistory());
                    PowerHost oldHost = (PowerHost) vm.getHost();oldHost.vmDestroy(vm);
                    PowerHostUtilizationHistory srcHost = (PowerHostUtilizationHistory) oldHost;
                    TargetParams.put(srcHost.getId(), srcHost.getUtilizationHistory());

            }			
        }
}