/**
 * The Immediate Overload Detection Algorithm (IOD)
 *
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 *
 * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 * 
 * @author H. Monshizadeh Naeen
 */
package org.cloudbus.cloudsim.power;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;
import org.cloudbus.cloudsim.util.CUSUMs;
import org.cloudbus.cloudsim.util.MarkovChains;

public class MarkovVmAllocationPolicyMigrationIOD extends PowerVmAllocationPolicyMigrationAbstract {

	/** The Allowable OTF. */
	private double AllowableOTF = 0.1;
        
	/** The scheduling interval. */
	private double schedulingInterval;


	/**
	 * Instantiates a new power vm allocation policy migration mad.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 * @param utilizationThreshold the utilization threshold
	 */
	public MarkovVmAllocationPolicyMigrationIOD(
			List<? extends Host> hostList,
			PowerVmSelectionPolicy vmSelectionPolicy,
			double utilizationThreshold) {
		super(hostList, vmSelectionPolicy);
		setAllowableOTF(AllowableOTF);
	}

	/**
	 * Checks if is host over utilized.
	 * 
	 * @param _host the _host
	 * @return true, if is host over utilized
	 */
	@Override
	protected boolean isHostOverUtilized(PowerHost _host) {
		double totalRequestedMips = 0;
		for (Vm vm : _host.getVmList()) {
			totalRequestedMips += vm.getCurrentRequestedTotalMips();
		}
		double utilization = totalRequestedMips / _host.getTotalMips();
                //---------Gets OTF Value on the host-->>>
		double slaViolationTimePerHost = 0;
		double totalTime = 0;
                double ro=getAllowableOTF();
                  if(utilization > 1){
			HostDynamicWorkload host = (HostDynamicWorkload) _host;
			double previousTime = -1;
			double previousAllocated = 0;
			double previousRequested = 0;
			boolean previousIsActive = true;
			for (HostStateHistoryEntry entry : host.getStateHistory()) {
				if (previousTime != -1 && previousIsActive) {
					double timeDiff = entry.getTime() - previousTime;
					totalTime += timeDiff;
                                      	if (previousAllocated < previousRequested) {
						slaViolationTimePerHost += timeDiff;
					}
				}
				previousAllocated = entry.getAllocatedMips();
				previousRequested = entry.getRequestedMips();
				previousTime = entry.getTime();
				previousIsActive = entry.isActive();
			}
                        addHistoryEntry(host, slaViolationTimePerHost);
                    }
                    else
                    {
                        PowerHostUtilizationHistory host = (PowerHostUtilizationHistory) _host;
                        double[] utilizationHistory = host.getUtilizationHistory();
                        int length =utilizationHistory.length;
                        double[] utilizationHistoryReversed = new double[length];
                        for (int i = 0; i < length; i++) {
                              utilizationHistoryReversed[i] = utilizationHistory[length - i - 1];
                        } 
						// the following lines are defined based on the OTEST algorithm
                        int[] step_stLen_hsLen_lp=CUSUMs.getScaleCUSUMsData(utilizationHistoryReversed);
                        int chP=step_stLen_hsLen_lp[0];
                        int stLen=step_stLen_hsLen_lp[1];
                        int hsLen=step_stLen_hsLen_lp[2];
                        int lp=step_stLen_hsLen_lp[3];
                        if(stLen<lp)
                        { //immadiate detection cannot be done so we use deffered policy
                              return false;
                        }
                        double[] NewUtilizationHistoryAfterAllocation =CUSUMs.selectArrFrom(utilizationHistoryReversed, chP, hsLen);
                        int N=11;;//emperical results showed that 11 states have a good result
                        int[] obsN=MarkovChains.make_n_state_observation_sequence(NewUtilizationHistoryAfterAllocation, N);
                        double[][] transitionPbMatrixN=MarkovChains.transitionProbabilityMatrix(obsN, N);
                        double[][] StN=MarkovChains.StationaryDistribution_PowerMethod(transitionPbMatrixN, N);;
                        double pbN=StN[N-1][0];
                        return (pbN>ro);
                  }
        return (slaViolationTimePerHost / totalTime)>ro;
        }

	/**
	 * Sets the Allowable OTF.
	 * 
	 * @param AllowableOTF the new utilization threshold
	 */
	protected void setAllowableOTF(double AllowableOTF) {
		this.AllowableOTF = AllowableOTF;
	}

	/**
	 * Gets the Allowable OTF.
	 * 
	 * @return the Allowable OTF
	 */
	protected double getAllowableOTF() {
		return AllowableOTF;
	}
        protected void setSchedulingInterval(double schedulingInterval) {
		this.schedulingInterval = schedulingInterval;
	}

	/**
	 * Gets the scheduling interval.
	 * 
	 * @return the scheduling interval
	 */
	protected double getSchedulingInterval() {
		return schedulingInterval;
	}

}
