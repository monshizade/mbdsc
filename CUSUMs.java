/**
 * The CUSUMs Class
 * 
 * The class includes CUSUM, StandardizedCUSUM, Scale CUSUM, and other related functions
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 * 
 * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 * 
 * @author H. Monshizadeh Naeen
 */
 

 
 
 
 
package org.cloudbus.cloudsim.util;

public class CUSUMs {
    public static double[] reverseArr(double[] arr)
    {
        int len= arr.length;
        double[] arrRev=new double[len];
        for(int i=0;i<len;i++)
        {
            arrRev[(len-1)-i]=arr[i];
        }
        return arrRev;
        
    }
    public static int changePoint(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//Counters
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double[] y=new double[30];
        double x1;
        double x2;
        h=h*initial_sigma;
        k=k*initial_sigma;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            x1=max(0,inputSequence[i]-(initial_mu+k)+x1);
            x2=max(0,(initial_mu-k)-inputSequence[i]+x2);
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    break;
            }
        }
        int res=(stepPoint-(N)) ;
        return res;
    }
     public static int changePointStandardizedCUSUM(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//Counters
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double y;
        double x1;
        double x2;
        h=h*initial_sigma;
        k=k*initial_sigma;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            x1=max(0,y-k+x1);
            x2=max(0,-k-y+x2);
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    break;
            }
        }
        int res=(stepPoint-(N)) ;
        return res;
    }
       public static int changePointScaleCUSUM(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//Counters
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double y,v;
        double x1;
        double x2;
        h=h*initial_sigma;
        k=k*initial_sigma;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            v=(Math.sqrt(Math.abs(y))-0.822)/0.349;
            x1=max(0,v-k+x1);
            x2=max(0,-k-v+x2);
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            //-------------------------------
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    break;
            }
        }
        int res=(stepPoint-(N)) ;
        return res;
    }
    public static int[] StandardizedCusumChangePoint(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//Counters
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double y;
        double x1;
        double x2;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            x1=max(0,y-k+x1);
            x2=max(0,-k-y+x2);
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    break;
            }
        }
        int[] res={stepPoint, N} ;
        return res;
    }
    public static int[] ScaleCusumChangePoint(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//Counters
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double y;
        double x1;
        double x2;
        double v;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            v=(Math.sqrt(Math.abs(y))-0.822)/0.349;
            x1=max(0,v-k+x1);
            x2=max(0,-k-v+x2);
                        //counter
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    break;
            }
        }
        int[] res={stepPoint, N} ;
        return res;
    }
    public static int[] StandardizedCusumChangePointPositiveOrNegative(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//shomarande mahale voghooe step
        int len=inputSequence.length, hPointP=0,hPointN=0, stepPoint=0;
        double y;
        double x1;
        double x2;
        x1=0.0;
        x2=0.0;
        int PorN=0;//0 not found 1=P and 2=N
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            x1=max(0,y-k+x1);
            x2=max(0,-k-y+x2);
                        //counter
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            //-------------------------------
            if (hPointP==0.0 && x1>h){           
                    stepPoint=hPointP=i;
                    N=NP;
                    PorN=1;
                    break;
            }
            else if (hPointN==0.0 && x2>h){
                    stepPoint=hPointN=i;
                    N=NN;
                    PorN=2;
                    break;
            }
        }
        int[] res={stepPoint - N, PorN} ;
        return res;
    }
    public static int[] StandardizedVariableCusumChangePoint(double[] inputSequence,int startPoint, double k , double h,  double initial_mu, double initial_sigma )
    {
        int NP=0,NN=0,N=0;//shomarande mahale voghooe step
        int len=inputSequence.length, vPointP=0,vPointN=0, stepPoint=0,vPoint=0;
        double y,v;
        double x1,s1;
        double x2,s2;
        x1=0.0;
        x2=0.0;
        for(int i=startPoint+1;i<len;i++)
        {
            y=(inputSequence[i]-initial_mu)/initial_sigma;
            v=(Math.sqrt(Math.abs(y))-0.822)/0.349;
            x1=max(0,y-k+x1);
            x2=max(0,-k-y+x2);
            s1=max(0,v-k+x1);
            s2=max(0,-k-v+x2);
            if(x1>0)
            {
                NP++;
                NN=0;
            }
            else if(x2>0)
            {
                NN++;
                NP=0;
            }
            else
            {
                NP=NN=0;
            }
            //-------------------------------
            if (x1>h){  
                if (s1>h||s2>h)
                    vPoint=i;
                    stepPoint=i;
                    N=NP;
                    break;
            }
            else if (x2>h){
                 if (s1>h||s2>h)
                    vPoint=i;
                    stepPoint=i;
                    N=NN;
                    break;
            }
             if (vPointP==0.0 && s1>h){  
                    vPoint=i;
                    vPointP=i;
                    N=NP;
                    break;
            }
            else if (vPointN==0.0 && s2>h){
              
                    vPoint=i;
                    vPointP=i;
                    N=NN;
                    break;
            }
        }
        int[] res={stepPoint, vPoint ,N} ;
        return res;
    }    
    public static double max(double x, double y)
    {
        if(x>=y)
            return x;
        else
            return y;
    }
    public static double mean(double[] y) {
		double sum = 0;
		for (Double number : y) {
			sum += number;
		}
		return sum / y.length;
	}
    public static double variance(double[] list) {
		long n = 0;
		double mean = mean(list);
		double s = 0.0;

		for (double x : list) {
			n++;
			double delta = x - mean;
			mean += delta / n;
			s += delta * (x - mean);
		}
		return s / (n - 1);
	}
    public static double stDev(double[] list) {
		return Math.sqrt(variance(list));
	}
    public static double[] selectArrFrom(double[] arr,int from, int to)
    {
        if(from<0)
            from=0;
        double[] output=new double[to-from];
        for(int i=0;i<(to-from);i++)
        {
            output[i]=arr[i+from];
        }
        return output;
    }
    public static int[] getScaleCUSUMsData(double[] utilizationHistory)
    {
                        int hsLen=utilizationHistory.length;
                        int stLen=hsLen;//the length that workload is stationary
                        int from=0, to=30;// the learning phase length is set to 30
                        if(hsLen<30)
                            to=hsLen;
                        int stPoint=0;
                        int chP=0,prevChP=0;
                        int lp=to;//learning phase
                        double[] learningData=new double[lp];
                        while(stLen>=lp)
                        {
                            learningData=CUSUMs.selectArrFrom(utilizationHistory, from, to);
                            chP=CUSUMs.changePointScaleCUSUM(utilizationHistory,stPoint,1.5, 5, CUSUMs.mean(learningData), CUSUMs.stDev(learningData));
                           
                            if(chP>0)
                            {
                                chP++;
                                stLen=hsLen-chP;
                                stPoint=from=chP;
                                to=chP+lp;
                                prevChP=chP;
                            }
                            else
                            {
                                 stLen=hsLen-prevChP;
                                 chP=prevChP;
                                 break;
                            }
                            
                        }
                        int[] res={chP,stLen,hsLen,lp};
                        return res;
    }
    public static int[] getStandardizedCUSUMsData(double[] utilizationHistory)
    {
                        int hsLen=utilizationHistory.length;
                        int stLen=hsLen;//the length that workload is stationary
                        int from=0, to=30;// the learning phase length is set to 30
                        if(hsLen<30)
                            to=hsLen;
                        int stPoint=0;
                        int chP=0,prevChP=0;
                        int lp=to;//learning phase
                        double[] learningData=new double[lp];
                        while(stLen>=lp)
                        {
                            learningData=CUSUMs.selectArrFrom(utilizationHistory, from, to);
                            chP=CUSUMs.changePointScaleCUSUM(utilizationHistory,stPoint,1.5, 5, CUSUMs.mean(learningData), CUSUMs.stDev(learningData));
                           
                            if(chP>0)
                            {
                                chP++;
                                stLen=hsLen-chP;
                                stPoint=from=chP;
                                to=chP+lp;
                                prevChP=chP;
                            }
                            else
                            {
                                 stLen=hsLen-prevChP;
                                 chP=prevChP;
                                 break;
                            }                            
                        }
                        int[] res={chP,stLen,hsLen,lp};
                        return res;
    }
     public static int[] getStandardizeCUSUMsData(double[] utilizationHistoryAfterAllocation)
    {
                        int hsLen=utilizationHistoryAfterAllocation.length;
                        int stLen=hsLen;//the length that workload is stationary
                        int from=0, to=30;// the learning phase length is set to 30
                        if(hsLen<30)
                            to=hsLen;
                        int stPoint=0;
                        int prevChP=0;
                        int[] chPP=new int[2];
                        int lp=to;//learning phase
                        double[] tmp=new double[lp];
                        int possitiveShift=0;
                        while(stLen>=lp)
                        {
                            tmp=CUSUMs.selectArrFrom(utilizationHistoryAfterAllocation, from, to);
                            chPP=StandardizedCusumChangePointPositiveOrNegative(utilizationHistoryAfterAllocation,stPoint,1.5, 5, CUSUMs.mean(tmp), CUSUMs.stDev(tmp));                        
                            if(chPP[0]>0)
                            {
                                chPP[0]++;
                                stLen=hsLen-chPP[0];
                                stPoint=from=chPP[0];
                                to=chPP[0]+lp;
                                prevChP=chPP[0];
                                possitiveShift=chPP[1];
                            }
                            else
                            {
                                 stLen=hsLen-prevChP;
                                 chPP[0]=prevChP;
                                 break;
                            }                            
                        }
                        int[] res={chPP[0],stLen,hsLen,lp,possitiveShift};
                        return res;
    }
             public static double[] getCurrentStationaryWorkload(double[] utilizationHistory)
        {
            int[] step_stLen_hsLen_lp=CUSUMs.getScaleCUSUMsData(utilizationHistory);
            int chP=step_stLen_hsLen_lp[0];
            int stLen=step_stLen_hsLen_lp[1];
            int hsLen=step_stLen_hsLen_lp[2];
            int lp=step_stLen_hsLen_lp[3];
            if(stLen<lp)
            { 
                chP=hsLen-30;//if utilization trace after Step Point is short, we use last 30 utilization history
            }
            double[] NewUtilizationHistoryAfterAllocation =CUSUMs.selectArrFrom(utilizationHistory, chP, hsLen);
            return (NewUtilizationHistoryAfterAllocation);
        }

    
}
