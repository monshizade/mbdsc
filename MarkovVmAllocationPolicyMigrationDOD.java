/**
 * The	Deferred  Overload Detection Algorithm (DOD)
 * 
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 * 
 * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 * 
 * @author H. Monshizadeh Naeen
 */

package org.cloudbus.cloudsim.power;

import java.util.List;

import org.cloudbus.cloudsim.Host;
import org.cloudbus.cloudsim.HostDynamicWorkload;
import org.cloudbus.cloudsim.HostStateHistoryEntry;
import org.cloudbus.cloudsim.Vm;

public class MarkovVmAllocationPolicyMigrationDOD extends PowerVmAllocationPolicyMigrationAbstract {

	/** The utilization threshold. */
	private double AllowableOTF = 0.1;

	/**
	 * Instantiates a new power vm allocation policy migration mad.
	 * 
	 * @param hostList the host list
	 * @param vmSelectionPolicy the vm selection policy
	 * @param utilizationThreshold the utilization threshold
	 */
	public MarkovVmAllocationPolicyMigrationDOD(
			List<? extends Host> hostList,
			PowerVmSelectionPolicy vmSelectionPolicy,
			double utilizationThreshold) {
		super(hostList, vmSelectionPolicy);
		setAllowableOTF(utilizationThreshold);
	}

	/**
	 * Checks if is host over utilized.
	 * 
	 * @param _host the _host
	 * @return true, if is host over utilized
	 */
	@Override
	protected boolean isHostOverUtilized(PowerHost _host) {
		double totalRequestedMips = 0;
		for (Vm vm : _host.getVmList()) {
			totalRequestedMips += vm.getCurrentRequestedTotalMips();
		}
		double utilization = totalRequestedMips / _host.getTotalMips();      
		double slaViolationTimePerHost = 0;
		double totalTime = 0;
                double ro=getAllowableOTF();
                  if(utilization > 1){
			HostDynamicWorkload host = (HostDynamicWorkload) _host;
			double previousTime = -1;
			double previousAllocated = 0;
			double previousRequested = 0;
			boolean previousIsActive = true;                       
			for (HostStateHistoryEntry entry : host.getStateHistory()) {
				if (previousTime != -1 && previousIsActive) {
					double timeDiff = entry.getTime() - previousTime;
					totalTime += timeDiff;                                        
					if (previousAllocated < previousRequested) {                                        
						slaViolationTimePerHost += timeDiff;                                               
					}
				}

				previousAllocated = entry.getAllocatedMips();
				previousRequested = entry.getRequestedMips();
				previousTime = entry.getTime();
				previousIsActive = entry.isActive();
			}
                addHistoryEntry(host, slaViolationTimePerHost);                           
	}
        return (slaViolationTimePerHost / totalTime)>ro;
        }
        

	/**
	 * Sets the utilization threshold.
	 * 
	 * @param utilizationThreshold the new utilization threshold
	 */
	protected void setAllowableOTF(double AllowableOTF) {
		this.AllowableOTF = AllowableOTF;
	}

	/**
	 * Gets the utilization threshold.
	 * 
	 * @return the utilization threshold
	 */
	protected double getAllowableOTF() {
		return AllowableOTF;
	}

}
