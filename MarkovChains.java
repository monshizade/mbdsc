/**
 * The MarkovChains Class
 * 
 * The class includes Markov-chains related and needed functions
 * If you are using any algorithms or policies included in the Markov package, please cite the following paper:
 * 
  * Monshizadeh Naeen H, Zeinali E, Toroghi Haghighat A.
 * Adaptive Markov-based Approach for Dynamic Virtual Machine Consolidation in Cloud Data Centers with Quality of Service Constraints
 * Softw: Pract Exper. 2019;1–23. https://doi.org/10.1002/spe.2764
 * 
 * @author H. Monshizadeh Naeen
 */
package org.cloudbus.cloudsim.util;

import Jama.Matrix;

public class MarkovChains {
    
    public static int[] make_2_state_observation_sequence(double[] hist)
    {
        int len=hist.length;
        int[] obs=new int[len];
        for (int i=0;i<len;i++)
        {
            if(hist[i]>=1)
                obs[i]=1;
            else
                obs[i]=0;
        }
        return obs;
    }
    public static int[] make_3_state_observation_sequence(double[] hist, double perecent)
    {
        int len=hist.length;
        int[] obs=new int[len];
        for (int i=0;i<len;i++)
        {
            if(hist[i]>=1)
                obs[i]=2;
            else if(hist[i]>perecent)
                obs[i]=1;
            else 
                obs[i]=0;
        }
        return obs;
    }
    public static int[] make_n_state_observation_sequence(double[] hist, int n)
    {
        int len=hist.length;
        int[] obs=new int[len];
        for( int i=0;i<len;i++)
        {
            if(hist[i]>=1)
                obs[i]=n-1;
            else
                obs[i]=(int)(hist[i]*(n-1));
        }
        return obs;
    }
    //n is the number of possible states
    public static double[][] transitionProbabilityMatrix(int[] obs, int n)
    {
        int[][] trans = new int[n][n];
        for(int i=0;i<obs.length-1;i++)
        {
            trans[obs[i]][obs[i+1]]=trans[obs[i]][obs[i+1]]+1;
        }    
        double[][] transProb=new double[n][n];
        for(int i=0;i<n;i++)
        {
            int sum=0;
            for(int j=0;j<n;j++)
            {
             sum+=trans[i][j];   
            }   
            for(int j=0;j<n;j++)
            {
                if(sum>0)
                transProb[i][j]=(double)trans[i][j]/sum;
                else
                    transProb[i][j]=0;
            }
        }
        return transProb;
    }
    public static double[][] StationaryDistribution_PowerMethod(double[][] res, int N)
    {
        Matrix A = new Matrix(res);
       
        A = A.transpose();
        Matrix x = new Matrix(N, 1, 1.0 / N); 
        for (int i = 0; i < 50; i++) {
            x = A.times(x);
            x = x.times(1.0 / x.norm1());       // rescale
        }
        double[][] SM=x.getArray();
        return SM;
        
    }
    
}